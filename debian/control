Source: xymon
Section: net
Priority: optional
Maintainer: Axel Beckert <abe@debian.org>
Uploaders: Christoph Berg <myon@debian.org>,
           Roland Rosenfeld <roland@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-apache2,
               imagemagick,
               libc-ares-dev,
               libldap2-dev,
               libpcre2-dev,
               librrd-dev,
               libssl-dev,
               libtirpc-dev,
               man2html-base,
               po-debconf,
               procps
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/debian/xymon.git
Vcs-Browser: https://salsa.debian.org/debian/xymon
Homepage: http://xymon.sourceforge.net/
Rules-Requires-Root: no

Package: xymon
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: xymon-client (>= 4.3.30-2),
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: ${misc:Recommends}
# The relation with rename is only for file-rename in postinst script
Suggests: ntpsec-ntpdig,
          rename,
          rrdtool
Description: monitoring system for systems, networks and applications
 Xymon (previously called Hobbit) is a network- and applications-
 monitoring system designed for use in large-scale networks. But it will
 also work just fine on a small network with just a few nodes. It is
 low-overhead and high-performance, with an easy to use web front-end.
 .
 Network services are monitored remotely. Machine health and
 statistics are monitored through a locally installed client package
 (xymon-client). Additional (remote as well as local) checks are
 available in the package hobbit-plugins.
 .
 Alerts can trigger when monitoring detects a problem, resulting in
 e-mails or calls to your pager or mobile phone.
 .
 Xymon has a great deal of inspiration from the non-free Big Brother
 package, but does not include any Big Brother code.

Package: xymon-client
Architecture: any
Pre-Depends: debconf | debconf-2.0,
             ${misc:Pre-Depends}
Depends: adduser,
         lsb-release,
         net-tools,
         procps,
         ${misc:Depends},
         ${shlibs:Depends}
Breaks: xymon (<< 4.3.30-2)
Replaces: xymon (<< 4.3.30-2)
Suggests: xymon-plugins | hobbit-plugins
Description: client for the Xymon network monitor
 Client data collection package for Xymon (previously known as Hobbit).
 .
 This gathers statistics and data from a single system and reports it to
 the Xymon monitor. You should install this package on all systems if you
 have a Xymon server running.
 .
 Additional checks are available in the package hobbit-plugins.
